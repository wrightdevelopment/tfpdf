<?php namespace tFPDF;

use Illuminate\Support\ServiceProvider;
use tFPDF\lib\tFPDF;

class tFPDFServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('tFPDF', function ($app) {
            return new tFPDF;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['tFPDF'];
    }

}
