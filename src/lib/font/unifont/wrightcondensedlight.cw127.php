<?php
$rangeid=122;
$prevcid=126;
$prevwidth=544;
$interval=false;
$range=array (
  32 => 
  array (
    0 => 222,
    1 => 533,
    2 => 509,
  ),
  35 => 
  array (
    0 => 544,
    1 => 544,
    'interval' => true,
  ),
  37 => 
  array (
    0 => 878,
    1 => 711,
    2 => 298,
  ),
  40 => 
  array (
    0 => 322,
    1 => 322,
    'interval' => true,
  ),
  42 => 
  array (
    0 => 544,
    1 => 544,
    'interval' => true,
  ),
  44 => 
  array (
    0 => 322,
    1 => 378,
    2 => 322,
    3 => 433,
  ),
  48 => 
  array (
    0 => 562,
    1 => 562,
    'interval' => true,
    2 => 562,
    3 => 562,
    4 => 562,
    5 => 562,
    6 => 562,
    7 => 562,
    8 => 562,
    9 => 562,
  ),
  58 => 
  array (
    0 => 322,
    1 => 322,
    'interval' => true,
  ),
  60 => 
  array (
    0 => 544,
    1 => 544,
    'interval' => true,
    2 => 544,
  ),
  63 => 
  array (
    0 => 489,
    1 => 900,
    2 => 656,
  ),
  66 => 
  array (
    0 => 600,
    1 => 600,
    'interval' => true,
  ),
  68 => 
  array (
    0 => 656,
    1 => 544,
    2 => 489,
    3 => 656,
    4 => 656,
    'interval' => true,
  ),
  73 => 
  array (
    0 => 322,
    1 => 544,
    2 => 600,
    3 => 489,
    4 => 878,
    5 => 711,
    6 => 656,
    7 => 544,
    8 => 656,
  ),
  82 => 
  array (
    0 => 600,
    1 => 600,
    'interval' => true,
  ),
  84 => 
  array (
    0 => 544,
    1 => 656,
    2 => 600,
    3 => 933,
  ),
  88 => 
  array (
    0 => 600,
    1 => 600,
    'interval' => true,
  ),
  90 => 
  array (
    0 => 544,
  ),
  91 => 
  array (
    0 => 322,
    1 => 322,
    'interval' => true,
    2 => 322,
  ),
  94 => 
  array (
    0 => 544,
    1 => 600,
    2 => 378,
    3 => 544,
    4 => 544,
    'interval' => true,
  ),
  99 => 
  array (
    0 => 489,
  ),
  100 => 
  array (
    0 => 544,
    1 => 544,
    'interval' => true,
  ),
  102 => 
  array (
    0 => 378,
  ),
  103 => 
  array (
    0 => 544,
    1 => 544,
    'interval' => true,
  ),
  105 => 
  array (
    0 => 322,
    1 => 322,
    'interval' => true,
  ),
  107 => 
  array (
    0 => 489,
    1 => 322,
    2 => 767,
  ),
  110 => 
  array (
    0 => 544,
    1 => 544,
    'interval' => true,
    2 => 544,
    3 => 544,
  ),
  114 => 
  array (
    0 => 378,
    1 => 489,
    2 => 378,
    3 => 544,
    4 => 489,
    5 => 767,
  ),
  120 => 
  array (
    0 => 489,
    1 => 489,
    'interval' => true,
  ),
  122 => 
  array (
    0 => 433,
    1 => 451,
    2 => 322,
    3 => 451,
    4 => 544,
  ),
);
?>