<?php namespace tFPDF;

use Illuminate\Support\Facades\Facade;

class tFPDF extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'tFPDF';
    }

}
